﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Kazka.DAL
{
    public class KazkaDbContext : DbContext
    {
        public DbSet<Models.News> AllNews { get; set; }

        public KazkaDbContext(DbContextOptions<KazkaDbContext> options) : base(options)
        {
            //Database.EnsureCreated();
        }
    }
}
