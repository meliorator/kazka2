﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kazka.DAL
{
    [Table("news")]
    public class News
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

        [Column("descript")]
        public string Body { get; set; }

        [Column("created_at")]
        public DateTime CreatedAt { get; set; }

        [Column("file_name")]
        public string FileName { get; set; }

        [Column("is_archive")]
        public int IsArchive { get; set; }
    }
}
