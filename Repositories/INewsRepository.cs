﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kazka.Models;

namespace Kazka.Repositories
{
    public interface INewsRepository
    {
        IEnumerable<News> GetNews();
    }
}
