﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kazka.DAL;
using Kazka.Models;

namespace Kazka.Repositories
{
    public class NewsRepository : INewsRepository
    {
        private readonly KazkaDbContext _db;
        
        public NewsRepository(KazkaDbContext db)
        {
            _db = db;            
        }

        public IEnumerable<Models.News> GetNews()
        {
            return _db.AllNews;
        }
    }
}
